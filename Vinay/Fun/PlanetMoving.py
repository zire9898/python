#just execute the program PlanetMoving.py
#just a small "plante moving"-project. I wanted to show less than metastable systems.
#If one mass is a little bit different the system wont be symmetric anymore
#its not gravitation...

#if it is symmetric there should be a point at 0,0


import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation

global tau, rangeX, rangeY

setOneOff = 0 #if =1 two planets wont be symmetric and the whole system will collide - the point in the middle
              #should move (it is mass midpoint)

tau = 0.005 #time for plot
rangeX = 5 #field
rangeY = 5
VarSpeed = 1 #speed
VarMass = 1000 #mass (can be positive and negative)
VarPos = 1 #start position range

N=7 #N*2 planets


class Planet:
    def __init__(self, pos, speed, mass):

        self.pos = pos
        self.speed = speed
        self.mass = mass
        self.force = np.array([0, 0])

    def CreateSymmetric(self):
        return Planet(-1*self.pos, -1*self.speed, self.mass)

    def Move(self):
        self.force = self.force/self.mass
        self.pos = self.pos + self.speed*tau + 0.5*self.force*(tau**2)
        self.speed = self.speed + self.force * tau
        self.force = np.array([0, 0])
        self.HitEdge()

    def HitEdge(self):
        if np.abs(self.pos[0]) > rangeX:
            if self.pos[0]>0:
                self.pos[0] = rangeX
            else:
                self.pos[0] = -rangeX
            self.speed[0] = -1 * self.speed[0]

        if np.abs(self.pos[1]) > rangeY:
            if self.pos[1]>0:
                self.pos[1] = rangeY
            else:
                self.pos[1] = -rangeY
            self.speed[1] = -1 * self.speed[1]





def Distance(Planet1, Planet2):
    return np.sqrt((Planet1.pos[0]-Planet2.pos[0])**2+(Planet1.pos[1]-Planet2.pos[1])**2)


def Direction(Planet1, Planet2):
    return (Planet2.pos-Planet1.pos)/Distance(Planet1, Planet2)


def Force(Planet1, Planet2):
    return (Planet2.mass*Direction(Planet1, Planet2)/(Distance(Planet1, Planet2)))



def AllForce(Planet1, AllPlanets):
    for Planet2 in AllPlanets:
        if Distance(Planet1, Planet2) > 0.1:
            F = Force(Planet1, Planet2)
            Planet1.force = Planet1.force + F

def Center(AllPlanets):
    p = np.array([0, 0])
    m = 0
    for P in AllPlanets:
        p = p + P.pos*P.mass
        m = m + P.mass

    p = p/m

    return p

AllPlanets = []
for i in range(N):
    pos = np.array([VarPos*np.random.rand(), VarPos*np.random.rand()])
    speed = np.array([VarSpeed*np.random.randn(), VarSpeed*np.random.randn()])
    mass = np.random.randn()*VarMass
    P = Planet(pos, speed, mass)
    P2 = P.CreateSymmetric()
    AllPlanets.append(P)
    AllPlanets.append(P2)

if setOneOff == 1:
    AllPlanets[0].mass = 0

fig = plt.figure()
ax = plt.axes(xlim=(-rangeX, rangeX), ylim=(-rangeY, rangeY))
line, = ax.plot([], [], 'o', lw=2)

def init():
    line.set_data([], [])
    return line,

def animate(i):
    x = []
    y = []

    for P in AllPlanets:
        AllForce(P, AllPlanets)

    for P in AllPlanets:
        P.Move()
        x.append(P.pos[0])
        y.append(P.pos[1])

    C = Center(AllPlanets)
    x.append(C[0])
    y.append(C[1])

    line.set_data(x, y)
    return line,



anim = animation.FuncAnimation(fig, animate, init_func=init, frames=200, interval=20, blit=True)

plt.show()
