#solves knight-problem (knight starts at Start (0, 0) and tries to hit every panel of the chess field exactly once
#run Knight.py first window will show the solution and the second one will show how it works.

#blue - set
#red - position
#yellow - delete


import numpy as np
from graphics import *


N = 5 #5x5 field
Start = [0, 0] #start
Height = 600
Width = 600

timewait = .4 #time wait for draw

DrawStep = Height/N
Radius = DrawStep/4

dx = [[2, 1, -1, -2, -2, -1, 1, 2], [1, 2, 2, 1, -1, -2, -2, -1]] #possible moves



def DrawField(win):
    c = 0
    for i in range(N):
        for j in range(N):

            R = Rectangle(Point(DrawStep * i, DrawStep * j), Point(DrawStep * (i + 1), DrawStep * (j + 1)))

            if (c == 0):
                R.setFill('white')

            else:
                R.setFill('black')

            R.draw(win)

            c += 1
            c = c % 2

        if (N % 2 == 0):
            c += 1
            c = c % 2





def MoveKnight(pos, step, win, Draw, sol):
    if (step == N * N):
        return 1

    C = 0
    for i in range(len(dx[1][:])):

        if (C!=0 and Draw):
            C = Circle(Point(pos[0]*DrawStep+0.5*DrawStep, pos[1]*DrawStep+0.5*DrawStep), Radius)
            C.setFill('red')
            C.draw(win)
            time.sleep(timewait/2)
            C.undraw()

        Nextpos = (pos[0] + dx[0][i], pos[1] + dx[1][i])

        if(Nextpos[0] >= 0 and Nextpos[0] < N and Nextpos[1] >= 0 and Nextpos[1] < N and sol[Nextpos[0]][Nextpos[1]] == -1):

            sol[Nextpos[0]][Nextpos[1]] = step
            if(Draw):
                C = Circle(Point(Nextpos[0]*DrawStep+0.5*DrawStep, Nextpos[1]*DrawStep+0.5*DrawStep), Radius)
                C.setFill('red')
                C.draw(win)
                time.sleep(timewait)
                C.setFill('blue')
                message = Text(Point(Nextpos[0]*DrawStep+0.5*DrawStep, Nextpos[1]*DrawStep+0.5*DrawStep), step)
                message.draw(win)

            if (MoveKnight(Nextpos , step+1, win, Draw, sol) == 1):
                return 1
            else:
                sol[Nextpos[0]][Nextpos[1]]=-1
                if(Draw):
                    C.setFill('yellow')
                    time.sleep(timewait)
                    C.undraw()
                    message.undraw()
    return 0



def solveKnight(Start, Draw):

    sol = np.zeros([N, N]) - 1
    sol[Start[0]][Start[1]] = 0
    win = GraphWin('Face', Width, Height)

    if(Draw):

        DrawField(win)
        C = Circle(Point(Start[0]*DrawStep + 0.5 * DrawStep, Start[1]*DrawStep +0.5 * DrawStep), Radius)
        C.draw(win)
        C.setFill('red')
        time.sleep(timewait)
        C.setFill('blue')
        Massage = Text(Point(Start[0]*DrawStep + 0.5 * DrawStep, Start[1]*DrawStep +0.5 * DrawStep), 0)
        Massage.draw(win)

    if (MoveKnight([Start[0], Start[1]], 1, win, Draw, sol) == 0):
        print("Geht nicht!!")
        if (Draw):
            C.undraw()
            Massage.undraw()

    else:
        print(sol)
        if(Draw == 0):
            DrawSolution(sol, win)

    return win



def DrawSolution(sol, win):

    DrawField(win)

    for i in range(N):
        for j in range(N):
            T = Text(Point(i*DrawStep+0.5*DrawStep, j*DrawStep+0.5*DrawStep), int(sol[i][j]))
            T.setFill('blue')
            T.draw(win)


win = solveKnight(Start, 0)
win2 = solveKnight(Start, 1)

win.getMouse()
win.close()
