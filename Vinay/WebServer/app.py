from flask import Flask, render_template, request, flash, url_for, redirect, send_from_directory
import pygal
import io
import base64
import numpy as np
import os
from werkzeug.utils import secure_filename


import RandomWalk as RW
import speechThing as ST

UPLOAD_FOLDER = '/uploads'


app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/')
def index():
    return render_template('home.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/hidden')
def hidden():
    print("Someone got into our hidden spot!")
    return render_template('hidden.html')

@app.route('/graphing')
def graphing():
    N = 5
    Steps = 10
    graph_data = RW.randomwalk(N, Steps)
    return render_template("graphing.html", graph_data = graph_data)

@app.route('/graphing', methods=['POST'])
def graphing_post():
    N = request.form.get('NumberWalker', type=int)
    Steps = request.form.get('Steps', type=int)
    graph_data = RW.randomwalk(N, Steps)

    return render_template("graphing.html", graph_data = graph_data)

@app.route('/upload')
def upload_file():
   return render_template('upload.html')

@app.route('/upload', methods = ['GET', 'POST'])
def upload_file2():
   if request.method == 'POST':
      f = request.files['file']
      f.save(secure_filename(f.filename))
      wordsGer, wordsEn = ST.getWords(secure_filename(f.filename))
      print(wordsGer)
      print(wordsEn)
      return render_template('upload.html', wordsGer = wordsGer, wordsEn = wordsEn)



if __name__ == '__main__':
   app.run(debug = True)

if __name__ == '__main__':
    app.run(debug=True)
