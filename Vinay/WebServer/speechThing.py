import speech_recognition as sr

def getWords(filename):
    r = sr.Recognizer()
    with sr.AudioFile(filename) as source:
        audio = r.listen(source)

    StringGer = r.recognize_google(audio, language="de_DE")
    StringEn = r.recognize_google(audio, language="en_EN")

    return StringGer, StringEn
