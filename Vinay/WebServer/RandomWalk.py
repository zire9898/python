import pygal
import numpy as np

def randomwalk(N, xrange):
    graph = pygal.Line()
    graph.title = "Random Walk"


    for i in range(N):
        y = np.zeros(xrange+1)
        y[0] = 0
        r = 0
        for j in range(xrange):
            r += np.random.randn()
            y[j+1] = r
        graph.add('Walker %i' %(i+1), y)



    return graph.render_data_uri()
