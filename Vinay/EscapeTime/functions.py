import math
import numpy as np
from matplotlib import pyplot as plt

#Rotator Function
def RotatorNextPos(Pos):
    return np.array([(Pos[0] + Pos[1]) % (2*math.pi), (1-0.02)*Pos[1] + 3.5*math.sin(Pos[0]+Pos[1])])


#get 1 random Number out of a distribution
def getRandom(Name, Var):
    if Name == 'Uniform':
        return np.array([np.sqrt(Var*3)*2*(np.random.rand()-0.5), np.sqrt(Var*3)*2*(np.random.rand()-0.5)])
    elif Name == 'Normal':
        return np.array([np.sqrt(Var)*np.random.randn(), np.sqrt(Var)*np.random.randn()])
    else:
        return np.array([0, 0])


#get escape time - y-component criterium
def getEscapeTime(Distribution, Var, Start):
    ystart = 2*math.pi*Start
    pos = [math.pi, ystart]
    count = 0

    while pos[1] < ystart+math.pi and pos[1] > ystart - math.pi:
        pos = RotatorNextPos(pos) + getRandom(Distribution, Var)
        count += 1

    return count

#get mean escape time
def getMeanEscapeTime(Distribution, Var, Start, Runs):
    counts = 0
    for i in range(Runs):
        counts += getEscapeTime(Distribution, Var, Start)

    return counts/Runs

#get mean escape time for a range variance
def getMeanEscapeTimeVar(Distribution, VarMin, VarStep, VarMax, Start, Runs):
    VarInv = []
    tau = []

    while VarMin < VarMax:
        VarInv.append(1 / VarMin)
        tau.append(getMeanEscapeTime(Distribution, VarMin, Start, Runs))
        VarMin += VarStep

    return VarInv, tau

#get the pdf of the escape time for one variance
def getPdfEscapeTime(Distribution, Var, Start, Runs):
    t = []

    for i in range(Runs):
        counts = 0
        counts += getEscapeTime(Distribution, Var, Start)
        t.append(counts)


    return t

#creates a map of basin for the rotator
def getMap(N, xmin, xmax, xstep, ymin, ymax, ystep):
    Z = np.zeros([(int)((ymax-ymin)/ystep)+1, (int)((xmax-xmin)/xstep)+1])

    x=xmin
    y=ymin
    xcount = 0

    while x < xmax:
        y = ymin
        ycount = 0
        while y < ymax:
            pos = np.array([x, y])
            start = pos

            for i in range(N):
                pos = RotatorNextPos(pos)

            diff = np.sqrt((pos[0]-start[0])**2+(pos[1]-start[1])**2)

            Z[ycount][xcount] = diff

            y += ystep
            ycount += 1
        x += xstep
        xcount += 1

    xPos = [i*xstep+xmin for i in range((int)((xmax-xmin)/xstep)+1)]
    yPos = [i*ystep+ymin for i in range((int)((ymax-ymin)/ystep)+1)]


    return xPos, yPos, Z


#plots the basin of the rotator
def plotContour(N, xmin, xmax, xstep, ymin, ymax, ystep):
    X, Y, Z = getMap(N, xmin, xmax, xstep, ymin, ymax, ystep)
    CS = plt.contourf(X, Y, Z)
    plt.colorbar(CS)
    plt.title('Basin of rotator')
    plt.xlabel('x')
    plt.ylabel('y')

