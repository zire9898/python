#Run MeanEscape.py - functions.py needs to be imported
#plot basin of the rotator function, mean escape times for uniform and normal distributed random numbers


import functions
import math
from matplotlib import pyplot as plt

#plot the basin of the rotator
functions.plotContour(10, 0, 2*math.pi, 0.05, -2*math.pi, 2*math.pi, 0.05)

VarMin = 0.02
VarStep = 0.01
VarMax = 0.12
Runs = 1000
Start = 0
Distribution = 'Normal'

#mean escape time for uniform and normal distribution
VarInv, tau = functions.getMeanEscapeTimeVar('Normal', VarMin, VarStep, VarMax, Start, Runs)
T = functions.getPdfEscapeTime('Normal', VarMin, Start, Runs)

VarInv2, tau2 = functions.getMeanEscapeTimeVar('Uniform', VarMin, VarStep, VarMax, Start, Runs)
T2 = functions.getPdfEscapeTime('Uniform', VarMin, Start, Runs)


#plot these things
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_yscale("log")
P, = plt.plot(VarInv, tau, 'o')
P2, = plt.plot(VarInv2, tau2, 'o')
plt.legend([P, P2], ['Normal', 'Uniform'])
plt.xlabel('1/Varianz')
plt.ylabel('tau')

fig2 = plt.figure()
ax2 = fig2.add_subplot(2, 1, 1)
plt.hist(T, 50)
plt.xlabel('Escape Time Normal Distribution')
plt.ylabel('Probability')
ax3 = fig2.add_subplot(2, 1, 2)
plt.hist(T2, 50)
plt.xlabel('Escape Time Uniform Distribution')
plt.ylabel('Probability')

plt.show()